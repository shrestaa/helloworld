#!/bin/bash

export JAVA_HOME="/usr/share/jdk1.8.0_221"
export PATH=$PATH:${JAVA_HOME}/bin

now="$(date)"
#now="`/usr/bin/date`"
echo "Current date and time : $now"

echo "JAVA_HOME:" ${JAVA_HOME}

#echo "env: `env`"

cd MyJavaProject/ProjectShapes/src
${JAVA_HOME}/bin/javac Circle.java
if [ $? -eq 0 ]
then
    echo "compile worked!"
fi

